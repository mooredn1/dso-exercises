# DSO Course - Lab Exercise One

## Fork

### Step 1: Fork `flask_helloworld`

The first part of this DevSecOps Lab is the representative "dev" part.  In order to not focus too much on the details of software engineering or development we've provided a basic "helloworld" web application written in Python - so it should be fairly self explanatory and not too difficult to navigate.

To start, you will need to fork the [project repository](https://gitlab.com/gtpe-dso-lab/flask_helloworld). You can either press the `Fork` button in the top right corner of the repo's front page, or you can go here: https://gitlab.com/gtpe-dso-lab/flask_helloworld/-/forks/new.

Fork the repository to the subgroup you created in setup (in the `Create Your Subgroup` section).

## Pipeline tests

### Step 2: Fix the `test` stage

You will need to run the CI/CD pipeline once you create the fork. To do this, click on __CI / CD > Pipelines__ on the left-side navigation bar. From there press `Run Pipeline`. It will ask you to enter any required variables. You can skip this and press `Run Pipeline` again.

The pipeline should fail, because what fun would this exercise be if that was it. 

And your new mission is to resolve the build and test errors.

The `lint` and `unittest` stages will both fail.  Look into the logs for each and see if you can figure out how to resolve them.  If it isn't obvious, or the log output looks like Greek to you (assuming you don't read Greek) - there are clues sprinkled in the source code.  

Once you get the "Test" stage passing the "Scan" will start (and fail).  Fix that one too.

Once "Scan" is fixed the "Package" stage will fail. That is fine. We will address that in Exercise 2. 

## Hints, But Don't Start Here...

Take a look in the source and test files: `helloworld.py` and `helloworld_test.py`.  Fix, commit and push if you did the clone step.

## Extra time?

### Step 3: BONUS: Vulnerability Check

Look into the dependencies defined in `requirements.txt` and see if you can find any related vulnerabilities associated with them - perhaps searching the National Vulnerability Database (NVD)at: https://nvd.nist.gov/

Consider how you might be able to scan for vulnerabilities against those defined dependencies within the pipeline. Feel free to implement a solution to add this scanning. The `safety` python package and the `snyk` service are both resonable options. 
