# DSO Course - Lab Exercise Two

## Environment Variables

To get the next part of your pipeline working you will need to setup a few project-specific environment variables in GitLab. 

The details for adding these variables is below, but first you need you get your personal access token from GitLab. 

The `gtpe-dso-lab` group (which your subgroup is a part) has access to the following environmental variables which will be used by your pipelines.

| Environment Variable |
|----------------------|
| `CI_USER` |
| `CI_PASSWORD` |
| `DOCKER_USERNAME` |
| `DOCKER_PASSWORD` |

### Step 1: Get your personal access token

You need to create a Personal Access Token to give your pipeline access to push the git tag to your repo. You will need to add this to your repo's CI/CD variables.

Go to your settings ("Edit profile"):

![Settings](images/Kit_Plummer___flask_helloworld_·_GitLab.png)

Select "Access Token":

![Access Token](images/Edit_Profile_·_User_Settings_·_GitLab.png)

Enter a name for your new token and select `write_repository` and `read_repository` for the scope.

![Create Token](images/Personal_Access_Tokens_·_User_Settings_·_GitLab-2.png)

Copy the new token. You are going to need it in a minute. Note: once you leave the Access Tokens page you will no longer be able to see your token. So, **be sure you copy it**. 

![Created Token](images/Personal_Access_Tokens_·_User_Settings_·_GitLab.png)

### Step 2: Create your environment variables

You will need to add `APP_REPO_USER` and `APP_REPO_PASSWORD` to the fork created in your personal subgroup.

|Environmental Variable|Description|Location|
|----------------------|-----------|--------|
|`APP_REPO_USER`|Your GitLab username. This is needed so your pipeline can write to your git repo.|gtpe-dso-lab/YYYY-MM/your_username/flask_helloworld|
|`APP_REPO_PASSWORD`|Your personal access token which you created above. This is needed so your pipeline can write to your git repo.|gtpe-dso-lab/YYYY-MM/your_username/flask_helloworld|
|`CI_USER`|This is `dsolab`. This account will provide the ability to write to the `dso-deployment` repo. This was created by the course instructors.|gtpe-dso-lab - already added|
|`CI_PASSWORD`| Needed to provide access to `dso-deployment` repo. This was created by the course instructors.|gtpe-dso-lab - already added|
|`DOCKER_USERNAME`|This is `devsecopslab`. This is the account you will use to push your container to Docker Hub. This was created by the course instructors.|gtpe-dso-lab - already added|
|`DOCKER_PASSWORD`|Needed for Docker Hub so you can push your container. This was created by the course instructors.|gtpe-dso-lab - already added|

First, go to your fork's page at Gitlab.com:

![GitLab Fork](images/gitlab_fork_vars.png)

Here you'll click on the `Settings` option in the left-side navigation column, then click `CI/CD` to reveal the `Variables` section.  Click the `Expand` button for `Variables`, then `Add Variable` for each of the listed variables above using the dialog:

![Add variables to pipeline configuration](images/add_variable2.png)

Leave the `Flags` options as the defaults.

When you are finished your variables should look like this. 

![CI/CD Variables](images/gitlab_variables_added.png)

## Protected Branch

### Step 3: Protect the `develop` branch

Make the `develop` branch a "protected" branch by going into the Repository Settings view, selecting "Protected Branches" and adding `develop`.  The variables you set above will only work on protected branches.

![Adding a protected branch](images/protected_branch.png)

## Rename Stuff

In order to ensure your version doesn't whack other student's forks we need to rename this project something special and appending your student number in the appropriate places is the easiest way to do this.

### Step 4: Rename your project

In the `.gitlab-ci.yml` on line #7 if you append the student number to the `PROJECT_NAME:` definition that's it.

So make:

```
PROJECT_NAME: "flask-helloworld"
```

into

```
PROJECT_NAME: "flask-helloworld-<your_student_number>"
```

That's it, but remember what you set this too.  You'll need it again in a moment. 

## Run the Pipeline Again and Fix Again

### Step 5: Fix your packaged app

You should have made it through the `package` stage. This stage built your app as an OCI container. 

The next stage is the `packagescan` stage which scans the container. 

New broken stuffs, let's continue debugging the pipeline by troubleshooting from the stage logs in the pipeline.  

What's up with the package? Some vulnerabilities?  Not a difficult fix.

HINT: Focus on the line that says "This OS version is no longer ..."

Once you address this issue, your pipeline will make it to `deploy staging` stage where it will fail again. This is addressed in the next section.. 

## Deployment Time

### Step 6: Deploy your app to staging

Once you're ready to deploy, it is time to update the configuration-as-code found here: https://gitlab.com/gtpe-dso-lab/dso-deployment.  Follow the instructions on the [dso-deployment README](https://gitlab.com/gtpe-dso-lab/dso-deployment/-/blob/main/README.md). 

As soon as your app's configuration has been merged into the upstream repo, coordinate with the instructors to add your application to the ArgoCD configuration in the staging environment.

Once your app has been added to ArgoCD, your app will be deployed to staging. The instructors will provide the IP address for your staging deployment, so you can see your app running.